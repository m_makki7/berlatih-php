<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //
    public function register(){
        return view('register');
    }

    public function kirim(Request $request){
            $namadepan = $request->depan;
            $namabelakang = $request->belakang;
            return view('selamat',compact("namadepan","namabelakang"));
    }

    public function selamat(){
        return view('selamat');
    }
}
