<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'HomeController@home');
Route::get('/register', 'AuthController@register');
Route::post('/kirim','AuthController@kirim');
Route::get('/selamat', 'AuthController@selamat');
Route::get('/master',function(){
    return view('adminlte.master');
});

Route::get('/items',function(){
    return view('items.index');
});

Route::get('/items/create',function(){
    return view('items.create');
});

ROute::get('/',function(){
    return view('table_progress');
});

ROute::get('/data-tables',function(){
    return view('table_features');
});

Route::get('/pertanyaan/create','PertanyaanController@create');
Route::post('/pertanyaan','PertanyaanController@store');
Route::get('/pertanyaan','PertanyaanController@index');
Route::get('/pertanyaan/{id}','PertanyaanController@show');
Route::get('pertanyaan/{id}/edit','PertanyaanController@edit');
Route::PUT('/pertanyaan/{id}','PertanyaanController@update');
Route::delete('/pertanyaan/{id}','PertanyaanController@destroy');